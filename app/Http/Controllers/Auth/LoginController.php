<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function process_login(Request $request)
    {        
        $credentials = $request->except(['_token']);

        $user = User::where('first_name',$request->name)->first();

        if (auth()->attempt($credentials)) {
            return response()->json($user);
        }else{
            return response()->json(['status' => 'False', 'message' => 'No user data present with us please register first!']);            
        }
    }

    public function process_signup(Request $request)
    {   
         
        $user = User::create([
            'first_name' => trim($request->input('name')),
            'contact_number1' => trim($request->input('contact_number1')),
            'email' => strtolower($request->input('email')),
            'password' => bcrypt($request->input('password')),
        ]);

        return response()->json(['status' => 'True', 'message' => 'Your account is created!']);
       
    }

    public function logout()
    {
        \Auth::logout();

        return response()->json(['status' => 'True', 'message' => 'Log out successfully!']); 
    }

}
