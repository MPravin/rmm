import Vue from 'vue'
import Router from 'vue-router'
const DefaultContainer = () => import('../components/main')
// const About = () => import('../components/about')
// const Profile = () => import('../components/profile')

Vue.use(Router)

export default new Router({
    mode: 'history', // so required!
    linkActiveClass: 'open active',
    scrollBehavior: () => ({
        y: 0
    }),
    routes: [
        //   {
        //   path: '/',
        //   //redirect: '/login',
        //   name: 'Auth',
        //   component: Auth,

        // },

        {
            path: '/',
            name: 'Login',
            component: DefaultContainer,
        },
        // {
        //     path: '/about',
        //     name: 'about',
        //     component: About,
        // },
        // {
        //     path: '/profile',
        //     name: 'profile',
        //     component: Profile,
        // }
        // {
        //     path: '/manage/admin/',
        //     name: 'MasterLogin',
        //     component: MasterLogin,
        //     // path: '/',
        //     // redirect: '/buyer/dashboard',
        //     // name: 'Dash',
        //     // meta: { requiresAuth: true },
        // }
    ]
})
